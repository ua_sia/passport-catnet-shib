
"use strict;"

/*
    UW Shibboleth Passport Authentication Module

    This module exposes a passport Strategy object that is pre-configured to
    work with the UW's Shibboleth identity provider (IdP). To use this, you
    must register your server with the UW IdP, and you can use the
    metadataRoute() method below to provide the metadata necessary for
    registration via the standard metadata url (urls.metadata).

    author: Dave Stearns

    Modified for use at The University of Arizona by Gary Windham
*/

const passport = require('passport');
const saml = require('passport-saml');
const util = require('util');

const IdPCert = 'MIIDbTCCAlWgAwIBAgIUCPDhQQiByqJWLk5GmCp6MO669DwwDQYJKoZIhvcNAQELBQAwKzEpMCcGA1UEAwwgc2hpYmJvbGV0aC5raXR0ZW5uZXQuYXJpem9uYS5lZHUwHhcNMTcwMzA4MjAwNTEwWhcNMzcwMzA4MjAwNTEwWjArMSkwJwYDVQQDDCBzaGliYm9sZXRoLmtpdHRlbm5ldC5hcml6b25hLmVkdTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANj+b3ySYI1bTaeGjmuecH8l85B6goVChzswC31G6KfljMq2KO8M844rqHwpjKoKBRUI+32PWZAlcJfz2zpYEIZsUfR6PLukqpDV3cztEI7CQp+w8EcPHWWcRM6aP/X4CWvQzGewwCITUpF2Ni5CYAdntPEv+d2hsBXiQJqNlYCYGEaLoYwUP54XO6AEPWWjr772wzQ73rFQr5lCiEEx6JXyyWn5nfyj3jm6PGEayXKH2LUVbUSeEEWm5qY6OowX+nz3ls9gGRG/0iHmFaa4uzhHJHLfuI0d8Wvlv1oEujKWsZEyterCSl2a714Z2ZIu/oiBoN+RSa9KCXR/DOHhdv0CAwEAAaOBiDCBhTAdBgNVHQ4EFgQUpv/i66KQeh9+jVbDmVZbNMJHFjswZAYDVR0RBF0wW4Igc2hpYmJvbGV0aC5raXR0ZW5uZXQuYXJpem9uYS5lZHWGN2h0dHBzOi8vc2hpYmJvbGV0aC5raXR0ZW5uZXQuYXJpem9uYS5lZHUvaWRwL3NoaWJib2xldGgwDQYJKoZIhvcNAQELBQADggEBAEhaWStmD7b/HFX4s6A+Wp9l3Ek3Llzdo/7FmLAyw7WyrArEf9KTnc6EK5Wvaza/eB5BkRaHt+1/Ai5NpJCMMsN78jwhLRWpdIl7/K08sbM7aW4ZjouBg557cg8cgpihZXf7raz0pGHm4gRNPl67jjUiM4X+DC8vK1kQoeNFquKW1APIX+xoshbwmZEkrUUKVyMQAwBH2ksyDkJddEuxhfISEuCmz2mJ4dwWYYF6o5VgDt8qW9+/jj5Tqu1+I6328i1AOWFQ02ND1fU45WZbRlOy/tFiGCsanteMnq1xuzWLFsXPJnNBu6vej3UvwEfarvnd7y66JQb0RRayPNna/1I=';
const IdPEntryPoint = 'https://shibboleth.kittennet.arizona.edu/idp/profile/SAML2/Redirect/SSO';
const strategyName = 'catnetsaml';

//standard login, callback, logout, and meta-data URLs
//these will be exposed from module.exports so that
//clients can refer to them
//the metadata one in particular is important to get right
//as the auto-regisration process requires that exact URL
const urls = {
    metadata: '/saml/Metadata',
    uaLogoutUrl: 'https://shibboleth.kittennet.arizona.edu/idp/profile/Logout'
};

//export the urls map
module.exports.urls = urls;

//map of possible profile attributes and what name
//we should give them on the resulting user object
//add to this with other attrs if you request them
const profileAttrs = {
    'urn:oid:0.9.2342.19200300.100.1.1': 'Shib-uid'
};

function convertProfileToUser(profile) {
    var user = {};
    var niceName;
    var attr;
    for (attr in profile) {
        niceName = profileAttrs[attr];
        if (niceName !== undefined && profile[attr]) {
            user[niceName] = profile[attr];
        }
    }
    return user;
}

/*
    Passport Strategy for UW Shibboleth Authentication
    This class extends passport-saml's Strategy, providing the necessary
    options and handling the conversion of the returned profile into a
    sensible user object.

    options should contain:
        entityId: your server's entity id,
        domain: your server's domain name,
        callbackUrl: login callback url (relative to domain),
        privateKey: your private key for signing requests (optional)
*/
function Strategy(options) {
    samlOptions = {
        entryPoint: IdPEntryPoint,
        cert: IdPCert,
        identifierFormat: null,
        issuer: options.entityId || options.domain,
        callbackUrl: 'https://' + options.domain + options.callbackUrl,
        decryptionPvk: options.privateKey,
        privateCert: options.privateKey,
        acceptedClockSkewMs: 180000
    };

    function verify(profile, done) {
        if (!profile)
            return done(new Error('Empty SAML profile returned!'));
        else {
            user = convertProfileToUser(profile);
            if (user) {
                if ("authz" in options) {
                    authz = options["authz"](user);
                    if (!authz.status) {
                        return done(null, false, { message: authz.message });
                    }
                }
            }
            return done(null, user);
        }
    }

    saml.Strategy.call(this, samlOptions, verify);
    this.name = strategyName;
}

util.inherits(Strategy, saml.Strategy);

//expose the Strategy
module.exports.Strategy = Strategy;

/*
    Route implementation for the standard Shibboleth metadata route
    usage:
        var uashib = require(...);
        var strategy = new uashib.Strategy({...});
        app.get(uashib.urls.metadata, uashib.metadataRoute(strategy, myPublicCert));
*/
module.exports.metadataRoute = function(strategy, publicCert) {
    return function(req, res) {
        res.type('application/xml');
        res.status(200).send(strategy.generateServiceProviderMetadata(publicCert));
    }
} //metadataRoute

/*
    Middleware for ensuring that the user has authenticated.
    You can use this in two different ways. If you pass this to
    app.use(), it will secure all routes added after that.
    Or you can use it selectively on routes that require authentication
    like so:
        app.get('/foo/bar', ensureAuth(loginUrl), function(req, res) {
            //route implementation
        });

    where loginUrl is the url to your login route where you call
    passport.authenticate()
*/
module.exports.ensureAuth = function(loginUrl) {
    return function(req, res, next) {
        if (req.isAuthenticated())
            return next();
        else {
            req.session.authRedirectUrl = req.url;
            res.redirect(loginUrl);
        }
    }
};

/*
    Middleware for ensuring that the user has authenticated.
    You can use this in two different ways. If you pass this to
    app.use(), it will secure all routes added after that.
    Or you can use it selectively on routes that require authentication
    like so:
        app.get('/foo/bar', ensureAuthXHR(responseObj), function(req, res) {
            //route implementation
        });

    where responseObj is a Javascript object representing the JSON response to
    return to the XmlHttpRequest (with 401 status code) indicating that authentication failed.
*/
module.exports.ensureAuthXHR = function(responseObj) {
    return function(req, res, next) {
        if (req.isAuthenticated())
            return next();
        else {
            if (responseObj) {
              res.status(401).json(responseObj);
            } else {
              res.status(401).send({ error: 'authentication failed' });
            }
        }
    }
};

/*
    Middleware for redirecting back to the originally requested URL after
    a successful authentication. The ensureAuth() middleware above will
    capture the current URL in session state, and when your callback route
    is called, you can use this to get back to the originally-requested URL.
    usage:
        var uashib = require(...);
        var strategy = new uashib.Strategy({...});
        app.get('/login', passport.authenticate(strategy.name));
        app.post('/login/callback', passport.authenticate(strategy.name), uashib.backtoUrl());
        app.use(uashib.ensureAuth('/login'));
*/
module.exports.backToUrl = function(defaultUrl) {
    return function(req, res) {
        var url = req.session.authRedirectUrl;
        delete req.session.authRedirectUrl;
        res.redirect(url || defaultUrl || '/');
    }
};
